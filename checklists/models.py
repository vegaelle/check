from django.db import models
from django.conf import settings
from django.utils.text import slugify
from django.shortcuts import reverse

from orderable.models import Orderable
import reversion

ANSWER_VALUES = (
    ('un', 'décoché'),
    ('ch', 'coché'),
    ('na', 'non applicable'),
)

EMOJI_ANSWER_VALUES = {
    'un': '❎',
    'ch': '☑️',
    'na': '❌',
}


@reversion.register()
class Category(Orderable):
    label = models.CharField(max_length=32, unique=True)
    slug = models.SlugField()
    description = models.TextField(blank=True)
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='date de création'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='date de modification'
    )

    def __str__(self):
        return self.label

    def save(self):
        self.slug = slugify(self.label)
        return super().save()

    def checklist_list(self):
        return self.checklist_set.all()

    class Meta(Orderable.Meta):
        verbose_name = 'catégorie'


@reversion.register()
class CheckList(Orderable):
    label = models.CharField(max_length=32)
    slug = models.SlugField()
    category = models.ForeignKey(
        Category, on_delete=models.PROTECT,
        verbose_name='catégorie'
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='date de création'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='date de modification'
    )

    def __str__(self):
        return self.label

    def save(self):
        self.slug = slugify(self.label)
        return super().save()

    class Meta(Orderable.Meta):
        unique_together = (
            ('label', 'category'),
        )
        verbose_name = 'checklist'
        verbose_name_plural = 'checklists'


@reversion.register()
class CheckBox(Orderable):
    label = models.CharField(max_length=32)
    slug = models.SlugField()
    description = models.TextField(blank=True)
    checklist = models.ForeignKey(
        CheckList, on_delete=models.PROTECT,
    )
    depends_of = models.ForeignKey(
        'CheckBox', on_delete=models.PROTECT,
        null=True, blank=True, verbose_name='dépend de'
    )
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='date de création'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='date de modification'
    )

    def __str__(self):
        return self.label

    def save(self):
        self.slug = slugify(self.label)
        return super().save()

    class Meta(Orderable.Meta):
        unique_together = (
            ('label', 'checklist'),
        )
        verbose_name = 'checkbox'
        verbose_name_plural = 'checkboxes'


@reversion.register()
class Session(models.Model):
    name = models.CharField(max_length=32, verbose_name='nom')
    slug = models.SlugField()
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name='propriétaire'
    )
    enabled_categories = models.ManyToManyField(
        Category,
        verbose_name='catégories choisies'
    )
    active = models.BooleanField(default=True, verbose_name='active')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='date de création'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='date de modification'
    )

    def __str__(self):
        return self.name

    def save(self):
        self.slug = slugify(self.name)
        return super().save()

    def get_absolute_url(self):
        return reverse('session', kwargs={'slug': self.slug})

    def categories_list(self):
        return self.enabled_categories.all()

    def get_checkboxes(self):
        return CheckBox.objects.filter(
            checklist__category__in=self.enabled_categories.all()
        )

    def checked_boxes(self):
        return Answer.objects.filter(
            session=self,
            value__in=('ch', 'na'),
        ).count()

    def total_boxes(self):
        return self.get_checkboxes().count()

    def get_checkboxes_for_category(self, category):
        return CheckBox.objects.filter(
            checklist__category=category
        )

    def checked_boxes_for_category(self, category):
        return Answer.objects.filter(
            session=self,
            value__in=('ch', 'na'),
            checkbox__checklist__category=category,
        ).count()

    def total_boxes_for_category(self, category):
        return self.get_checkboxes_for_category(category).count()

    def get_checkboxes_for_checklist(self, checklist):
        return checklist.checkbox_set.all()

    def checked_boxes_for_checklist(self, checklist):
        return Answer.objects.filter(
            session=self,
            value__in=('ch', 'na'),
            checkbox__checklist=checklist,
        ).count()

    def total_boxes_for_checklist(self, checklist):
        return self.get_checkboxes_for_checklist(checklist).count()

    def save_answer(self, checkbox, value, comment=None):
        if checkbox not in self.get_checkboxes():
            raise ValueError(
                'Checkbox {} does not belong to session {}'
                .format(checkbox, self)
            )
        answer = Answer.objects.get_or_create(session=self, checkbox=checkbox)
        answer.value = value
        if comment:
            answer.comment = comment
        answer.save()
        return answer

    class Meta:
        unique_together = (
            ('name', 'user'),
        )


@reversion.register()
class Answer(models.Model):
    session = models.ForeignKey(Session, on_delete=models.PROTECT)
    checkbox = models.ForeignKey(CheckBox, on_delete=models.PROTECT)
    value = models.CharField(
        max_length=2, choices=ANSWER_VALUES,
        verbose_name='valeur'
    )
    comment = models.TextField(blank=True, verbose_name='commentaires')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='date de création'
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='date de modification'
    )

    def __str__(self):
        return '[{session}] {checkbox}: {value}'.format(
            session=self.session,
            checkbox=self.checkbox,
            value=EMOJI_ANSWER_VALUES[self.value]
        )

    class Meta:
        unique_together = (
            ('session', 'checkbox'),
        )
        verbose_name = 'réponse'
        verbose_name_plural = 'réponses'
