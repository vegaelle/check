from django.contrib import admin

from orderable.admin import OrderableAdmin, OrderableTabularInline
from reversion.admin import VersionAdmin

from .models import Category, CheckList, CheckBox, Session, Answer


class CheckListInline(admin.StackedInline):
    model = CheckList
    exclude = ('sort_order', 'slug', )


class CheckBoxInline(OrderableTabularInline):
    model = CheckBox
    exclude = ('sort_order', 'slug', )


class AnswerInline(admin.TabularInline):
    model = Answer


@admin.register(Category)
class CategoryAdmin(OrderableAdmin, VersionAdmin):
    list_display = (
        'sort_order_display', 'label', 'checklists', 'created_at',
        'updated_at',
    )
    inlines = [CheckListInline, ]
    exclude = ('sort_order', 'slug', )
    list_display_links = ('label', )

    def checklists(self, obj):
        return obj.checklist_set.count()


@admin.register(CheckList)
class CheckListAdmin(OrderableAdmin, VersionAdmin):
    list_display = (
        'sort_order_display', 'label', 'category', 'checkboxes', 'created_at',
        'updated_at',
    )
    inlines = [CheckBoxInline, ]
    exclude = ('sort_order', 'slug', )
    list_display_links = ('label', )

    def checkboxes(self, obj):
        return obj.checkbox_set.count()


@admin.register(CheckBox)
class CheckBoxAdmin(OrderableAdmin, VersionAdmin):
    list_display = (
        'sort_order_display', 'label', 'created_at', 'updated_at',
    )
    exclude = ('sort_order', 'slug', )
    list_display_links = ('label', )


@admin.register(Session)
class SessionAdmin(VersionAdmin):
    list_display = ('name', 'user', 'answers', 'created_at', 'updated_at', )
    inlines = [AnswerInline, ]
    exclude = ('slug', )
    list_display_links = ('name', )

    def answers(self, obj):
        return obj.answer_set.count()
    answers.verbose_name = 'réponses'

    def categories(self, obj):
        return obj.enabled_categories_set.count()
    categories.short_description = 'catégories'


@admin.register(Answer)
class AnswerAdmin(VersionAdmin):
    list_display = (
        'checkbox', 'session', 'bool_value', 'created_at', 'updated_at'
    )
    list_display_links = ('checkbox', )

    def bool_value(self, obj):
        if obj.value == 'un':
            return False
        if obj.value == 'ch':
            return True
        else:
            return None
    bool_value.short_description = 'valeur'
    bool_value.boolean = True
    bool_value.admin_order_field = 'value'
