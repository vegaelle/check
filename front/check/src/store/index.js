import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    auth: {
      authenticated: false,
      login: null
    },
    sessions: [
      {
        id: 1,
        title: 'Test session',
        curChecks: 2,
        totalChecks: 5,
        checkLists: [1, 2]
      },
      {
        id: 2,
        title: 'Test session 2',
        curChecks: 2,
        totalChecks: 2,
        checkLists: [3]
      },
      {
        id: 3,
        title: 'Test session 3',
        curChecks: 0,
        totalChecks: 4,
        checkLists: [4, 5, 6, 7]
      }
    ],
    checkLists: [
      {
        id: 1,
        title: 'Checklist 1',
        curChecks: 1,
        totalChecks: 3,
        items: [1, 2, 3, 4]
      },
      {
        id: 2,
        title: 'checklist 2',
        curChecks: 1,
        totalChecks: 2,
        items: [5, 6]
      },
      {
        id: 3,
        title: 'checklist 3',
        curChecks: 2,
        totalChecks: 2,
        items: [7, 8]
      },
      {
        id: 4,
        title: 'checklist 4',
        curChecks: 0,
        totalChecks: 1,
        items: [9]
      },
      {
        id: 5,
        title: 'checklist 5',
        curChecks: 0,
        totalChecks: 1,
        items: [10]
      },
      {
        id: 6,
        title: 'checklist 6',
        curChecks: 0,
        totalChecks: 1,
        items: [11]
      },
      {
        id: 7,
        title: 'checklist 7',
        curChecks: 0,
        totalChecks: 1,
        items: [12]
      }
    ],
    items: [
      {
        id: 1,
        title: 'Item 1',
        checked: true,
        na: false,
        comment: 'Already done'
      },
      {
        id: 2,
        title: 'Item 2',
        checked: false,
        na: false,
        comment: ''
      },
      {
        id: 3,
        title: 'Item 3',
        checked: false,
        na: false,
        comment: ''
      },
      {
        id: 4,
        title: 'Item 4',
        checked: false,
        na: true,
        comment: 'No need for that'
      },
      {
        id: 5,
        title: 'item 5',
        checked: true,
        na: false,
        comment: ''
      },
      {
        id: 6,
        title: 'item 6',
        checked: false,
        na: false,
        comment: ''
      },
      {
        id: 7,
        title: 'item 7',
        checked: true,
        na: false,
        comment: ''
      },
      {
        id: 8,
        title: 'item 8',
        checked: true,
        na: false,
        comment: ''
      },
      {
        id: 9,
        title: 'item 9',
        checked: false,
        na: false,
        comment: ''
      },
      {
        id: 10,
        title: 'item 10',
        checked: false,
        na: false,
        comment: ''
      },
      {
        id: 11,
        title: 'item 11',
        checked: false,
        na: false,
        comment: ''
      },
      {
        id: 12,
        title: 'item 12',
        checked: false,
        na: false,
        comment: ''
      }
    ]
  },
  getters: {
    getSessions: state => {
      return state.sessions
    },
    getSession: (state) => (id) => {
      id = parseInt(id)
      return state.sessions.filter(item => item.id === id)[0]
    },
    getSessionLists: (state) => (session) => {
      return state.checkLists.filter(item => session.checkLists.indexOf(item.id) !== -1)
    },
    getList: (state) => (id) => {
      id = parseInt(id)
      return state.checkLists.filter(item => item.id === id)[0]
    },
    getListItems: (state) => (checkList) => {
      return state.items.filter(item => checkList.items.indexOf(item.id) !== -1)
    }
  },
  mutations: {
    checkItem: (state, id) => {
      state.items.filter(item => item.id === id)[0].checked = true
    },
    uncheckItem: (state, id) => {
      state.items.filter(item => item.id === id)[0].checked = false
    },
    disableItem: (state, id) => {
      state.items.filter(item => item.id === id)[0].checked = false
      state.items.filter(item => item.id === id)[0].na = true
    },
    enableItem: (state, id) => {
      state.items.filter(item => item.id === id)[0].na = false
    },
    setItemComment: (state, id, comment) => {
      state.items.filter(item => item.id === id)[0].comment = comment
    }
  },
  actions: {

  }
})
