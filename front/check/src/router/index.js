import Vue from 'vue'
import Router from 'vue-router'
import HomeView from '@/components/HomeView'
import SessionView from '@/components/SessionView'
import CheckListView from '@/components/CheckListView'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/session/:id',
      name: 'session',
      component: SessionView
    },
    {
      path: '/session/:id/checklist/:id2',
      name: 'checklist',
      component: CheckListView
    },
    {
      path: '/add',
      name: 'add',
      component: HomeView
    }
  ]
})
